require 'rubygems'
require "tk"
require "serialport"

port_str = "/dev/ttyUSB0" 
baud_rate = 38400
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE
height = 500
width = 1000

canvas = TkCanvas.new('height' => height,'width' => width)
sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
trap("INT") do 
	sp.close
	exit
end

start = 0
end_l = 0

TkcRectangle.new(canvas, 0, 0, width, height,
                 'outline' => 'black', 'fill' => 'black')


canvas.pack
Thread.new {
	while true
	TkcRectangle.new(canvas, 0, 0, width, height,'outline' => 'black', 'fill' => 'black')
		cnt = 0
		val_prew = 0
		while cnt < 250
		val = sp.read(2).unpack('S').first
		TkcLine.new(canvas, 
		((width / 250.0) * cnt - 1).to_i,
		height - ((height/1024.0) * val_prew).to_i,
		((width / 250.0) * cnt).to_i,
		height - ((height/1024.0) * val).to_i,
		'width' => '1',
		'fill' => 'red'
		)
		cnt += 1
		val_prew = val
		end
	end
}

Tk.mainloop
