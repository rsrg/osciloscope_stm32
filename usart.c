#include "usart.h"

volatile uint8_t recive_flag = 0;
volatile uint8_t usart_data;

static void usart_setup(void){
	rcc_periph_clock_enable(RCC_GPIOA);
	gpio_mode_setup(GPIOA, GPIO_MODE_AF,GPIO_PUPD_NONE,GPIO9);
	gpio_set_af(GPIOA,GPIO_AF7,GPIO9);
	rcc_periph_clock_enable(RCC_USART1);

	usart_set_baudrate(USART1, 19200);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX_RX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
	nvic_enable_irq(NVIC_USART1_EXTI25_IRQ);
	usart_enable_rx_interrupt(USART1);
	usart_enable(USART1);


}
void usart1_exti25_isr(void) {
    if (((USART_ISR(USART1) & USART_ISR_RXNE) != 0)) {
        recive_flag = 1;
        usart_data = USART1_RDR;
    }
}

static void send_string(uint8_t *c){
	for(uint8_t i = 0;i < strlen((char *)c);i++){
		usart_send_blocking(USART1,c[i]);
	}
}
//static void read_and_send_one(void){
//	char c[10];
//	ADC_CLK_TOUCH;
//	uint16_t data = GPIO_IDR(ADC_DATA_PORT) & 0x03FF;
//	itoa(data,c,10);
//	strcat(c,"\r\n");
//	send_string((uint8_t *)c);
//}

static void send_adc_data(void){
	char c[10];
	for(uint16_t i = 0;i < ADC_MEASURE_LENGTH;i++){
		itoa(adc_data[i],c,10);
		strcat(c,"\r\n");
		send_string(c);
	}
}
static void send_adc_data_raw(void){
	for(uint16_t i = 0;i < ADC_MEASURE_LENGTH;i++){
		usart_send_blocking(USART1,adc_data[i]);
		usart_send_blocking(USART1,(adc_data[i] >> 8) & 0xff);
	}
}
