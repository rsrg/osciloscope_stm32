#define STM32F3


#include <stdint.h>
#include <string.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/dac.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/cortex.h>
#include <libopencm3/cm3/nvic.h>

#include "ada_lcd.h"
#include "adc.h"

#define OSC_AXIS_COLOR 0x2a2a

#define OSC_AXIS_STEP_WIDTH 43
#define OSC_AXIS_STEP_HEIGHT 43
#define OSC_AXIS_WIDTH_COUNT 7
#define OSC_AXIS_HEIGHT_COUNT 5

#define OSC_DRAW_FIELD_START_Y 1
#define OSC_DRAW_FIELD_START_X 6
#define OSC_DRAW_FIELD_END_X OSC_DRAW_FIELD_START_X + OSC_AXIS_WIDTH_COUNT * OSC_AXIS_STEP_WIDTH + OSC_AXIS_WIDTH_COUNT
#define OSC_DRAW_FIELD_END_Y OSC_DRAW_FIELD_START_Y + OSC_AXIS_HEIGHT_COUNT * OSC_AXIS_STEP_HEIGHT + OSC_AXIS_HEIGHT_COUNT
#define OSC_DRAW_FIELD_HALF_X (OSC_DRAW_FIELD_END_X - OSC_DRAW_FIELD_START_X) / 2
#define OSC_DRAW_FIELD_HALF_Y (OSC_DRAW_FIELD_END_Y - OSC_DRAW_FIELD_START_Y) / 2

#define BUTTON_SELECT 1
#define BUTTON_PLUS 2
#define BUTTON_MINUS 3
#define BUTTON_HOLD 4

#define FUNCTION_DELAY 0
#define FUNCTION_MEASURE 1
#define FUNCTION_LEVEL 2
#define FUNCTION_TRIG 3
#define FUNCTION_TEST 4


#define SWITCH_1_PORT GPIOA
#define SWITCH_1_PIN GPIO1
#define SWITCH_2_PORT GPIOA
#define SWITCH_2_PIN GPIO2
#define SWITCH_3_PORT GPIOA
#define SWITCH_3_PIN GPIO3
#define SWITCH_4_PORT GPIOB
#define SWITCH_4_PIN GPIO15



static void delay_ms(uint32_t);
static void init_null_data(void);
static void switch_gpio_setup(void);
static void dac_setup(void);
void osc_clear_data_field(void);
uint16_t osc_detect_start(void);
void osc_draw_data(void);
void osc_draw_field_border(void);
void osc_draw_axis_lines(void);
void dac_set_val(uint8_t);
void rcc_clock_setup_hse(void);
void draw_top_panel(void);
void draw_right_panel(void);
void draw_left_panel(void);
void button_calculate(uint8_t);
uint8_t check_button(void);
static void set_test_signal(void);



