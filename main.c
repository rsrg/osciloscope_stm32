#include "main.h"

#define ADC_MEASURE_LENGTH 350
uint16_t adc_data[ADC_MEASURE_LENGTH];
//uint32_t dma_data[ADC_MEASURE_LENGTH];

typedef struct {
	uint8_t y0;
	uint8_t y1;
} osc_coordinates;

typedef struct {
	void (*read_data_function_ptr)(uint16_t len,uint16_t *data);
	char *label;
} read_data_struct;

typedef struct {
	uint8_t dac_val;
	char *label;
} measure_values_struct;

typedef struct {
	uint32_t presc;
	uint32_t period;
	uint32_t value;
	char *label;
} test_signal_struct;

const test_signal_struct test_signals[] = {
	{.label = "50Hz",.presc = 999,.period = 1439,.value = 719},
	{.label = "100Hz",.presc = 999,.period = 719,.value = 359},
	{.label = "200Hz",.presc = 999,.period = 359,.value = 179},
	{.label = "300Hz",.presc = 499,.period = 479,.value = 239},
	{.label = "400Hz",.presc = 499,.period = 359,.value = 179},
	{.label = "500Hz",.presc = 499,.period = 287,.value = 143},
	{.label = "1kHz",.presc = 499,.period = 143,.value = 71},
	{.label = "2kHz",.presc = 249,.period = 143,.value = 71},
	{.label = "5kHz",.presc = 99,.period = 143,.value = 71},
	{.label = "10kHz",.presc = 99,.period = 71,.value = 35},
	{.label = "50kHz",.presc = 19,.period = 71,.value = 35},
	{.label = "100kHz",.presc = 9,.period = 71,.value = 35},
	{.label = "1MHz",.presc = 1,.period = 35,.value = 17}
};
const uint8_t test_signals_count = sizeof(test_signals) / sizeof(test_signal_struct);
volatile int8_t current_test_signals_index = 6;



const measure_values_struct measure_values[] = {
	{.label = "4V",.dac_val = 0},
	{.label = "3V",.dac_val = 22},
	{.label = "1.5V",.dac_val = 60},
	{.label = "0.5V",.dac_val = 91},
	{.label = "0.3V",.dac_val = 115},
	{.label = "0.2V",.dac_val = 163}
};
const uint8_t measure_values_count = sizeof(measure_values) / sizeof(measure_values_struct);
volatile int8_t current_measure_value_index = 2;

const read_data_struct read_data_arr_str[] = {
	{.label = "0.5us",.read_data_function_ptr = &adc_read_05us},
	{.label = "1us",.read_data_function_ptr = &adc_read_1us},
	{.label = "2.5us",.read_data_function_ptr = &adc_read_2_5us},
	{.label = "5us",.read_data_function_ptr = &adc_read_5us},
	{.label = "10us",.read_data_function_ptr = &adc_read_10us},
	{.label = "50us",.read_data_function_ptr = &adc_read_50us},
	{.label = "0.1ms",.read_data_function_ptr = &adc_read_0_1ms},
	{.label = "0.5ms",.read_data_function_ptr = &adc_read_0_5ms},
	{.label = "1ms",.read_data_function_ptr = &adc_read_1ms},
	{.label = "10ms",.read_data_function_ptr = &adc_read_10ms},
	{.label = "20ms",.read_data_function_ptr = &adc_read_20ms}
};

const uint8_t read_function_count = sizeof(read_data_arr_str) / sizeof(read_data_struct);
volatile int8_t current_read_function_index = 8;

volatile osc_coordinates adc_data_double_buffer[OSC_DRAW_FIELD_END_X - OSC_DRAW_FIELD_START_X - 1];

volatile int16_t shift_level = 0;
volatile uint8_t current_function = 0;
volatile uint8_t hold  = 0;
volatile uint16_t trig_level_draw = (OSC_DRAW_FIELD_END_Y - OSC_DRAW_FIELD_START_Y) / 2.0;

static void delay_ms(uint32_t ms){
	 for (uint32_t i = 0; i < ms * 9000; i++) __asm__("nop");
}

static void init_null_data(void){
	for(uint16_t i = 0;i < ADC_MEASURE_LENGTH;i++) adc_data[i] = 0;
}

static void switch_gpio_setup(void){
	gpio_mode_setup(SWITCH_1_PORT, GPIO_MODE_INPUT,GPIO_PUPD_PULLUP, SWITCH_1_PIN);
	gpio_mode_setup(SWITCH_2_PORT, GPIO_MODE_INPUT,GPIO_PUPD_PULLUP, SWITCH_2_PIN);
	gpio_mode_setup(SWITCH_3_PORT, GPIO_MODE_INPUT,GPIO_PUPD_PULLUP, SWITCH_3_PIN);
	gpio_mode_setup(SWITCH_4_PORT, GPIO_MODE_INPUT,GPIO_PUPD_PULLUP, SWITCH_4_PIN);
	gpio_set(SWITCH_1_PORT,SWITCH_1_PIN);
	gpio_set(SWITCH_2_PORT,SWITCH_2_PIN);
	gpio_set(SWITCH_3_PORT,SWITCH_3_PIN);
	gpio_set(SWITCH_4_PORT,SWITCH_4_PIN);
}
uint8_t check_button(void){
	uint16_t ra = gpio_port_read(GPIOA);
	if(!(ra & SWITCH_1_PIN)){
		return 1;
	}
	if(!(ra & SWITCH_2_PIN)){
		return 2;
	}
	if(!(ra & SWITCH_3_PIN)){
		return 3;
	}
	uint16_t rb = gpio_port_read(GPIOB);
	if(!(rb & SWITCH_4_PIN)){
		return 4;
	}
	return 0;
}

static void dac_setup(void){
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_DAC1);
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG,GPIO_PUPD_NONE, GPIO4);
	dac_enable(CHANNEL_1);
}

void osc_clear_data_field(void){
	uint16_t x0 = OSC_DRAW_FIELD_START_X + 1,x1 = OSC_DRAW_FIELD_END_X - 1,y0 = OSC_DRAW_FIELD_START_Y + 1,y1 = OSC_DRAW_FIELD_END_Y-1;
	ada_lcd_fill_rect(x0,y0,x1,y1,BLACK);
}
uint16_t osc_detect_start(void){

	uint16_t height = (OSC_DRAW_FIELD_END_Y - OSC_DRAW_FIELD_START_Y - 1) * 2;
	double factor =  1024.0 / height;

	uint16_t mid = ((trig_level_draw - OSC_DRAW_FIELD_START_Y) + shift_level + OSC_DRAW_FIELD_HALF_Y ) * factor;
	for(uint16_t i = 10;i < ADC_MEASURE_LENGTH - (OSC_DRAW_FIELD_END_X - OSC_DRAW_FIELD_START_X - 5);i++)
		if(adc_data[i - 5] <= mid && adc_data[i + 5] > mid)
			return i;

	return 0;
}
void osc_draw_data(void){
	uint16_t start_from = osc_detect_start();
	uint16_t width = OSC_DRAW_FIELD_END_X - OSC_DRAW_FIELD_START_X - 1;
	uint16_t height = (OSC_DRAW_FIELD_END_Y - OSC_DRAW_FIELD_START_Y - 1) * 2;
	uint16_t field_height = OSC_DRAW_FIELD_END_Y - OSC_DRAW_FIELD_START_Y - 2;
	uint16_t x0,y0,y1;
	int16_t global_y00;
	int16_t global_y01;
	double factor = height / 1024.0;
	double field_height_div_2 = field_height / 2.0 + shift_level;

	for(uint16_t i = 0;i < width -1;i++){

		x0 = i + OSC_DRAW_FIELD_START_X + 1;

		ada_lcd_draw_line(x0,adc_data_double_buffer[i].y0,x0,adc_data_double_buffer[i].y1,BLACK);

		global_y00 = (int16_t)(factor * adc_data[i + start_from]) - field_height_div_2;
		global_y01 = (int16_t)(factor * adc_data[i + start_from + 1]) - field_height_div_2;
		if(global_y00 > field_height) global_y00 = field_height;
		if(global_y01 > field_height) global_y01 = field_height;
		if(global_y00 < 0) global_y00 = 0;
		if(global_y01 < 0) global_y01 = 0;

		y0 = global_y00 + OSC_DRAW_FIELD_START_Y + 1;
		y1 = global_y01 + OSC_DRAW_FIELD_START_Y + 1;
		adc_data_double_buffer[i].y0 = y0;
		adc_data_double_buffer[i].y1 = y1;

		ada_lcd_draw_line(x0,y0,x0,y1,RED);
	}
	osc_draw_axis_lines();
}

void osc_draw_field_border(void){
	ada_lcd_draw_rectanlge(OSC_DRAW_FIELD_START_X,OSC_DRAW_FIELD_START_Y,OSC_DRAW_FIELD_END_X,OSC_DRAW_FIELD_END_Y,GREEN);
}
void osc_draw_axis_lines(void){
	uint16_t y_with_shift = OSC_DRAW_FIELD_END_Y - 1;

	for(uint8_t i = 1;i < OSC_AXIS_WIDTH_COUNT;i++){
		uint16_t x = OSC_DRAW_FIELD_START_X + (OSC_AXIS_STEP_WIDTH + 1) * i + 1;
		ada_lcd_draw_line(
			x,
			OSC_DRAW_FIELD_START_Y + 1,
			x,
			y_with_shift,
			OSC_AXIS_COLOR
		);
	}
	uint16_t x_with_shift = OSC_DRAW_FIELD_END_X - 1;
	for(uint8_t i = 1;i < OSC_AXIS_HEIGHT_COUNT;i++){
		uint16_t y = OSC_DRAW_FIELD_START_Y + (OSC_AXIS_STEP_HEIGHT + 1) * i + 1;
		ada_lcd_draw_line(
			OSC_DRAW_FIELD_START_X + 1,
			y,
			x_with_shift,
			y,
			OSC_AXIS_COLOR
		);
	}
}
void dac_set_val(uint8_t d){
	dac_load_data_buffer_single(d,RIGHT8,CHANNEL_1);
}

void rcc_clock_setup_hse(void){
	/* Enable internal high-speed oscillator. */
	rcc_osc_on(RCC_HSE);
	rcc_wait_for_osc_ready(RCC_HSE);
	/* Select HSI as SYSCLK source. */
	rcc_set_sysclk_source(RCC_CFGR_SW_HSE); /* XXX: se cayo */
	rcc_wait_for_sysclk_status(RCC_HSE);

	rcc_osc_off(RCC_PLL);
	rcc_wait_for_osc_not_ready(RCC_PLL);
	rcc_set_pll_source(RCC_CFGR_PLLSRC_HSE_PREDIV);
	rcc_set_pll_multiplier(RCC_CFGR_PLLMUL_MUL9);
	/* Enable PLL oscillator and wait for it to stabilize. */
	rcc_osc_on(RCC_PLL);
	rcc_wait_for_osc_ready(RCC_PLL);
	/*
	 * Set prescalers for AHB, ADC, ABP1, ABP2.
	 * Do this before touching the PLL (TODO: why?).
	 */
	rcc_set_hpre(RCC_CFGR_HPRE_DIV_NONE);
	rcc_set_ppre2(RCC_CFGR_HPRE_DIV_NONE);
	rcc_set_ppre1(RCC_CFGR_HPRE_DIV_2);
	/* Configure flash settings. */
	//flash_set_ws(FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY_2WS);
	/* Select PLL as SYSCLK source. */
	rcc_set_sysclk_source(RCC_CFGR_SW_PLL); /* XXX: se cayo */
	/* Wait for PLL clock to be selected. */
	rcc_wait_for_sysclk_status(RCC_PLL);
	rcc_osc_off(RCC_HSI);

	/* Set the peripheral clock frequencies used. */
	rcc_ahb_frequency  = 72000000;
	rcc_apb1_frequency = 36000000;
	rcc_apb2_frequency = 72000000;
}


void draw_top_panel(void){
	char buffer[15];
	memset(buffer,0,10);
	uint16_t color;
	uint16_t clear_x_start = OSC_DRAW_FIELD_START_X,
			clear_x_end = XLAND_MAX,
			clear_y_start = OSC_DRAW_FIELD_END_Y+4,
	        clear_y_end = YLAND_MAX;

	ada_lcd_fill_rect(clear_x_start,clear_y_start,clear_x_end,clear_y_end,BLACK);

	if(current_function == FUNCTION_DELAY) color = BLUE;
	else color = RED;
	ada_lcd_draw_string(OSC_DRAW_FIELD_START_X,OSC_DRAW_FIELD_END_Y+4,read_data_arr_str[current_read_function_index].label,color);

	if(current_function == FUNCTION_MEASURE) color = BLUE;
	else color = RED;
	ada_lcd_draw_string(OSC_DRAW_FIELD_START_X + 60,OSC_DRAW_FIELD_END_Y+4,measure_values[current_measure_value_index].label,color);
	
	if(current_function == FUNCTION_TEST) color = BLUE;
	else color = RED;
	ada_lcd_draw_string(OSC_DRAW_FIELD_START_X + 120,OSC_DRAW_FIELD_END_Y+4,test_signals[current_test_signals_index].label,color);
}
void draw_right_panel(void){
	uint16_t color;
	uint16_t clear_x_start = OSC_DRAW_FIELD_END_X + 1,
			clear_x_end = XLAND_MAX,
			clear_y_start = 0,
			clear_y_end = YLAND_MAX;

	ada_lcd_fill_rect(clear_x_start,clear_y_start,clear_x_end,clear_y_end,BLACK);

	if(current_function == FUNCTION_TRIG) color = BLUE;
	else color = RED;

	ada_lcd_draw_string(OSC_DRAW_FIELD_END_X + 1,trig_level_draw - 2,"<",color);

}
void draw_left_panel(void){
	uint16_t color;
	uint16_t clear_x_start = 0,
			clear_x_end = OSC_DRAW_FIELD_START_X-2,
			clear_y_start = 0,
			clear_y_end = YLAND_MAX;

	ada_lcd_fill_rect(clear_x_start,clear_y_start,clear_x_end,clear_y_end,BLACK);

	if(current_function == FUNCTION_LEVEL) color = BLUE;
	else color = RED;

	ada_lcd_draw_string(OSC_DRAW_FIELD_START_X - 6,shift_level - 2 + OSC_DRAW_FIELD_HALF_Y,">",color);

}
void button_calculate(uint8_t button){
	if(button == BUTTON_SELECT){
		if(current_function == FUNCTION_DELAY) current_function = FUNCTION_MEASURE;
		else if(current_function == FUNCTION_MEASURE) current_function = FUNCTION_TEST;
		else if(current_function == FUNCTION_TEST) current_function = FUNCTION_LEVEL;
		else if(current_function == FUNCTION_LEVEL) current_function = FUNCTION_TRIG;
		else if(current_function == FUNCTION_TRIG) current_function = FUNCTION_DELAY;
	}
	if(current_function == FUNCTION_DELAY){
		if(button == BUTTON_PLUS) current_read_function_index ++;
		else if(button == BUTTON_MINUS) current_read_function_index --;
		if(current_read_function_index < 0) current_read_function_index = 0;
		if(current_read_function_index > read_function_count - 1) current_read_function_index = read_function_count - 1;
	}
	if(current_function == FUNCTION_MEASURE){
		if(button == BUTTON_PLUS) current_measure_value_index ++;
		else if(button == BUTTON_MINUS) current_measure_value_index --;
		if(current_measure_value_index < 0) current_measure_value_index = 0;
		if(current_measure_value_index > measure_values_count - 1) current_measure_value_index =  measure_values_count - 1;
		dac_set_val(measure_values[current_measure_value_index].dac_val);
	}
	if(current_function == FUNCTION_LEVEL){
		if(button == BUTTON_PLUS) shift_level += 3;
		else if(button == BUTTON_MINUS) shift_level -= 3;
		if(shift_level < -OSC_DRAW_FIELD_HALF_Y) shift_level = -OSC_DRAW_FIELD_HALF_Y;
		if(shift_level > OSC_DRAW_FIELD_HALF_Y) shift_level = OSC_DRAW_FIELD_HALF_Y;
	}
	if(current_function == FUNCTION_TRIG){
		if(button == BUTTON_PLUS) trig_level_draw += 3;
		else if(button == BUTTON_MINUS) trig_level_draw -= 3;
		if(trig_level_draw < OSC_DRAW_FIELD_START_Y-2) trig_level_draw = OSC_DRAW_FIELD_START_Y-2;
		if(trig_level_draw > OSC_DRAW_FIELD_END_Y-2) trig_level_draw = OSC_DRAW_FIELD_END_Y-2;
	}
	if(current_function == FUNCTION_TEST){
		if(button == BUTTON_PLUS) current_test_signals_index ++;
		else if(button == BUTTON_MINUS) current_test_signals_index --;
		if(current_test_signals_index < 0) current_test_signals_index = 0;
		if(current_test_signals_index > test_signals_count - 1) current_test_signals_index = test_signals_count-1;
		set_test_signal();
	}
	if(button == BUTTON_HOLD){
		hold = !hold;
	}else{
		draw_top_panel();
		draw_right_panel();
		draw_left_panel();
		if(current_function != FUNCTION_TEST){
			osc_clear_data_field();
		}
	}

}
static void set_test_signal(void){
	timer_set_prescaler(TIM1, test_signals[current_test_signals_index].presc);

	timer_set_period(TIM1, test_signals[current_test_signals_index].period);
	timer_set_oc_value(TIM1, TIM_OC2, test_signals[current_test_signals_index].value);
}
static void tim_test_signal_setup(void){
	rcc_periph_clock_enable(RCC_TIM1);
	rcc_periph_clock_enable(RCC_GPIOA);
	gpio_mode_setup(GPIOA, GPIO_MODE_AF,GPIO_PUPD_NONE,GPIO9);
	gpio_set_af(GPIOA,GPIO_AF6,GPIO9);
	set_test_signal();
	timer_set_oc_slow_mode(TIM1, TIM_OC2);
	timer_set_oc_mode(TIM1, TIM_OC2, TIM_OCM_PWM1);

	timer_enable_oc_output(TIM1, TIM_OC2);

	timer_enable_break_main_output(TIM1);

	timer_enable_counter(TIM1);
}
static void draw_spalsh(void){
	uint16_t left_margin = 50;
	uint16_t top_margin = 50;
	ada_lcd_draw_string(left_margin,YLAND_MAX - top_margin - 5,"OSCILOSCOPE by Strelok",GREEN);
	ada_lcd_draw_string(left_margin,YLAND_MAX - top_margin - 15,"VERSION 1.0",GREEN);	
}
//static set_dma(void){
//
//	gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT,GPIO_PUPD_NONE,GPIO9);
//	gpio_set_output_options(GPIOA,GPIO_OTYPE_PP,GPIO_OSPEED_50MHZ,GPIO9);
//	gpio_clear(GPIOA,GPIO9);
//	rcc_periph_clock_enable(RCC_DMA2);
//
//	timer_set_dma_on_update_event(TIM8);
//
//	dma_channel_reset(DMA2,DMA_CHANNEL1);
//	dma_set_peripheral_address(DMA2, DMA_CHANNEL1, (uint32_t) &GPIOB_IDR);
//	dma_set_read_from_peripheral(DMA2, DMA_CHANNEL1);
//	dma_enable_memory_increment_mode(DMA2, DMA_CHANNEL1);
//	dma_disable_peripheral_increment_mode(DMA2, DMA_CHANNEL1);
//	dma_set_memory_size(DMA2, DMA_CHANNEL1, DMA_CCR_MSIZE_32BIT);
//	dma_set_peripheral_size(DMA2, DMA_CHANNEL1, DMA_CCR_MSIZE_32BIT);
//	dma_enable_circular_mode(DMA2, DMA_CHANNEL1);
//
//	dma_set_memory_address(DMA2, DMA_CHANNEL1, (uint32_t) dma_data);
//	dma_set_number_of_data(DMA2, DMA_CHANNEL1, ADC_MEASURE_LENGTH * 4);
//	dma_enable_transfer_complete_interrupt(DMA2,DMA_CHANNEL1);
//
//	dma_enable_channel(DMA2, DMA_CHANNEL1);
//	//nvic_enable_irq(NVIC_DMA2_CHANNEL1_IRQ);
//}
//void dma2_channel1_isr(void){
//	if ((DMA2_ISR &DMA_ISR_TCIF1) != 0) {
//			DMA2_IFCR |= DMA_IFCR_CTCIF1;
//			gpio_clear(GPIOA,GPIO9);
//			//received = 1;
//	}
//}
//void tim1_up_tim16_isr(void){
//	if (timer_get_flag(TIM16, TIM_SR_UIF)) {
//		timer_clear_flag(TIM16, TIM_SR_UIF);
//		//gpio_toggle(GPIOA,GPIO9);
//	}
//}

int main(void){
	rcc_clock_setup_hse();
	adc_gpio_setup();
	switch_gpio_setup();
	init_null_data();

	ada_lcd_begin();
	ada_lcd_clear();

	dac_setup();
	dac_set_val(measure_values[current_measure_value_index].dac_val);

	draw_spalsh();
	delay_ms(2000);
	ada_lcd_clear();
	
	osc_draw_field_border();
	draw_top_panel();
	draw_right_panel();
	draw_left_panel();


	//set_dma();



	tim_test_signal_setup();

	uint8_t button,button_pressed = 0;


	while (1) {
//		for(uint16_t i = 0;i < ADC_MEASURE_LENGTH;i++){
//				adc_data[i] = dma_data[i];
//		}
		if(!hold){
			read_data_arr_str[current_read_function_index].read_data_function_ptr(ADC_MEASURE_LENGTH,adc_data);
		}

		osc_draw_data();
		//ada_lcd_draw_int(OSC_DRAW_FIELD_START_X + 180,OSC_DRAW_FIELD_END_Y+4,dma_data[10],GREEN),
		button = check_button();
		if(button && !button_pressed){
			button_calculate(button);
			delay_ms(1);
			button_pressed = 1;

		}else if(!button){
			button_pressed = 0;
		}
	};

	return 0;
}
